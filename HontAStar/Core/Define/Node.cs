﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Hont.AStar
{
    class Node
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }
        public int ID { get; set; }

        public Position Position { get { return new Position(X, Y, Z); } }

        /// <summary>
        /// h and g sum.
        /// </summary>
        public int F { get { return G + H; } }
        /// <summary>
        /// From begin node to current node move value.
        /// </summary>
        public int G { get; set; }
        /// <summary>
        /// To arrived point estimate(estimate allow through the wall, interface influence that result).
        /// </summary>
        public int H { get; set; }
        /// <summary>
        /// Simular as unity layer, distinguish difference 
        /// </summary>
        public int Mask { get; set; }
        public bool Walkable { get; set; }
        public int Cost { get; set; }
        public object UserData { get; set; }
        /// <summary>
        /// Save the last step info, convenient you back.
        /// </summary>
        public Node Parent { get; set; }


        public void Init(int g, int h, bool walkable, int cost)
        {
            this.G = g;
            this.H = h;
            this.Walkable = walkable;
            this.Cost = cost;
        }

        public Node(int x, int y, int z, int id)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.ID = id;
            this.Walkable = true;
        }

        public bool CheckMask(int mask)
        {
            if (mask == -1) return true;

            return (mask & this.Mask) == this.Mask;
        }

        public override string ToString()
        {
            return string.Format("X:{0} Y:{1} Z:{2} F:{3} G:{4} H:{5} Walkable:{6} Cost:{7}", X, Y, Z, F, G, H, Walkable, Cost);
        }
    }
}
