﻿using UnityEngine;
using System.Collections;

namespace Hont.AStar
{
    public enum DebugerNodeTypeEnum { CompareNode, NextNode, CloseNode }

    public interface IAStarStepDebuger
    {
        void OnPathfindingBegin(Position startPos, Position endPos);
        void OnToNextPoint(Position nextPoint, DebugerNodeTypeEnum pointType);
        void OnAddToClosedList(Position point);
    }
}
