﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont
{
    public static class OCTUtility
    {
        public static T MinItem<T>(IEnumerable<T> enumerable, Func<T,float> selector)
        {
            var result = default(T);
            var tmp = float.MaxValue;

            using (var handle = enumerable.GetEnumerator())
            {
                while (handle.MoveNext())
                {
                    var value = selector(handle.Current);
                    if (value < tmp)
                    {
                        tmp = value;
                        result = handle.Current;
                    }
                }
            }

            return result;
        }

        public static T MaxItem<T>(IEnumerable<T> enumerable, Func<T, float> selector)
        {
            var result = default(T);
            var tmp = float.MinValue;

            using (var handle = enumerable.GetEnumerator())
            {
                while (handle.MoveNext())
                {
                    var value = selector(handle.Current);
                    if (value > tmp)
                    {
                        tmp = value;
                        result = handle.Current;
                    }
                }
            }

            return result;
        }
    }
}
